import mpmath as mp
from matplotlib.pyplot import *
from numpy import ones, linspace, exp, tanh, cosh, mod, pi, sinh, arange, square
import random

N = 4
k = 1
random.seed()

"""
the exact analytical solution
"""
# U = []
# Cv_per_spin_analytical =[]
# Tem = []
# J = 1
# for T in linspace(5.01, 0.01, 10000):
#     q = 2*sinh(2*J/(k*T))/cosh(2*J/(k*T))**2
#     K = mp.ellipk(q**2)
#     E = mp.ellipe(q**2)
#     U.append(- J *(1+2/pi*(2*tanh(2*J/(k*T))**2-1)*K)/tanh(2*J/(k*T)))
#     Cv_per_spin_analytical.append(4*k/pi*(J/(k*T*tanh(2*J/(k*T)))**2)*(K-E-(1-tanh(2*J/(k*T))**2)*(pi/2+(2*tanh(2*J/(k*T))**2-1)*K)))
#     Tem.append(T)
#
#


"""
the simulation solution
"""
J = -1

def twoD_quantity(S):

    FirstTerm1 = 0
    FirstTerm2 = 0
    M = 0

    for j in range(0, N):
        for i in range(0, N):
            FirstTerm1 += S[i, j] * S[mod(i + 1, N), j]
            FirstTerm2 += S[i, j] * S[i, mod(j + 1, N)]
            M += S[i, j]

    energy = J * (FirstTerm1 + FirstTerm2)
    return energy, M



def sweep_stage(initial_state, Tem):

    thermo_steps = 10000
    for j in range(thermo_steps * N ** 2):

        test = initial_state
        x = int(N * random.random())
        y = int(N * random.random())
        E_difference = - 2 * J * test[x, y] * (
                    test[mod(x - 1, N), y] + test[mod(x + 1, N), y] + test[x, mod(y - 1, N)] + test[x, mod(y + 1, N)])
        p = exp((- E_difference) / (k * Tem))

        if p >= random.random():
            initial_state[x, y] *= -1
        else:
            continue
        if Tem < 2.27:
            file2.write(str(Tem) + " " + str(j) + " " + str(twoD_quantity(initial_state)[1]) + "\n")

    return initial_state


def average_quantity(filtered_state, Tem):
    E_avg = 0.0
    E_square_avg = 0.0
    M_avg = 0.0
    M_square_avg = 0.0
    Avg_steps = 2000

    for i in range(Avg_steps):

        for j in range(10 * N ** 2):

            test = filtered_state
            x = int(N * random.random())
            y = int(N * random.random())
            E_difference = -2 * J * test[x, y] * (
                        test[mod(x - 1, N), y] + test[mod(x + 1, N), y] + test[x, mod(y - 1, N)] + test[
                    x, mod(y + 1, N)])
            p = exp((- E_difference) / (k * Tem))
            if p >= random.random():
                filtered_state[x, y] *= -1
            else:
                continue

        E_in_equilibrium, M_eq = twoD_quantity(filtered_state)

        E_avg += E_in_equilibrium
        E_square_avg += E_in_equilibrium ** 2
        M_avg += M_eq
        M_square_avg += M_eq ** 2

    return E_avg / Avg_steps, E_square_avg / Avg_steps, M_avg / Avg_steps, M_square_avg / Avg_steps



file1 = open(r"C:\Users\a1896\OneDrive\Desktop\Comp_stat_phys\csp\ex01\2D_Ising_square_raw\N=" + str(N) + ".txt", "w+")
file2 = open(
    r"C:\Users\a1896\OneDrive\Desktop\Comp_stat_phys\csp\ex01\2D_Ising_square_raw\N=" + str(N) + " running M.txt", "w+")


state = -ones([N, N], int)
for i in linspace(5.01, 0.01, 100):
    filtered_state = sweep_stage(state, i)

    E, E_2, M, M_2 = average_quantity(filtered_state, i)
    Cv = (E_2 - E ** 2) / (k * i ** 2 * N ** 2)
    Kai = (M_2 - M ** 2) / (k * i * N ** 2)

    file1.write(str(i) + " " + str(E / N ** 2) + " " + str(Cv) + " " + str(abs(M) / N ** 2) + " " + str(Kai) + "\n")

    state = filtered_state

file1.close()
file2.close()



""""""""""
READ TXT FILE
"""""""""
filename = r"C:\Users\a1896\OneDrive\Desktop\Comp_stat_phys\csp\ex01\2D_Ising_square_raw\N=" + str(N) + ".txt"
T = []
E = []
Cv = []
Mag = []
M_sus = []
with open(filename, 'r') as file_to_read:
    data = file_to_read.readlines()
    for line in data:
        Tt, Energy, Specific_heat, Mz, Mz_sus = [float(i) for i in line.split()]
        T.append(Tt)
        E.append(Energy)
        Cv.append(Specific_heat)
        Mag.append(Mz)
        M_sus.append(Mz_sus)

xlabel('Temperature', fontsize=11)
ylabel('Thermal quantities for a' + str(N) + '*' + str(N) + ' system', fontsize=11)
# my_xticks = arange(0, 5.5, 0.5)
# my_yticks = arange(-2,2.5,0.5)
# xticks(my_xticks, fontsize = 9)
# yticks(my_yticks ,fontsize = 9)
# plot(Tem, U,color = 'r', label = 'Analytical Energy')
# plot(Tem, Cv_per_spin_analytical, color = 'g', label = 'Analytical Specific Heat')

plot(T, E, color='r', marker='^', fillstyle='none', linestyle='none', label='MC Energy')
plot(T, Cv, color='g', marker='^', fillstyle='none', linestyle='none', label='MC Specific Heat')
plot(T, Mag, color='b', marker='.', fillstyle='none', linestyle='none', label='MC Magnetization')
plot(T, M_sus, color='k', marker='.', fillstyle='none', linestyle='none', label='Susceptibility')

xlim(0, 5.1)
ylim(-2.1, 2)
# legend(loc = 'best')
show()
