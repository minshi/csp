import matplotlib.pyplot as plt
from numpy import linspace, exp, mod, zeros, pi, arccos, cos, sin, array
import numpy as np
import random
from tqdm import tqdm

random.seed()

"""
3D Heisenberg Cubic Metropolis single-spin-flip
"""
L = 8
d = 3
N = L ** d
k = 1
J = 1


def avg_over_sites(Sx, Sy, Sz):
    E = 0
    for i in range(L):
        for j in range(L):
            for k in range(L):
                E += -J * (Sx[i, j, k] * (Sx[mod(i + 1, L), j, k] + Sx[i, mod(j + 1, L), k] + Sx[i, j, mod(k + 1, L)])
                           + Sy[i, j, k] * (Sy[mod(i + 1, L), j, k] + Sy[i, mod(j + 1, L), k] + Sy[i, j, mod(k + 1, L)])
                           + Sz[i, j, k] * (
                                       Sz[mod(i + 1, L), j, k] + Sz[i, mod(j + 1, L), k] + Sz[i, j, mod(k + 1, L)]))
    Mx = np.sum(Sx)
    My = np.sum(Sy)
    Mz = np.sum(Sz)
    M_avg = np.sqrt(Mx ** 2 + My ** 2 + Mz ** 2)

    return E/N , M_avg/N


def nn_sum(Sx, Sy, Sz, x, y, z):
    result_x = Sx[mod(x - 1, L), y, z] + Sx[mod(x + 1, L), y, z] \
               + Sx[x, mod(y - 1, L), z] + Sx[x, mod(y + 1, L), z] \
               + Sx[x, y, mod(z - 1, L)] + Sx[x, y, mod(z + 1, L)]
    result_y = Sy[mod(x - 1, L), y, z] + Sy[mod(x + 1, L), y, z] \
               + Sy[x, mod(y - 1, L), z] + Sy[x, mod(y + 1, L), z] \
               + Sy[x, y, mod(z - 1, L)] + Sy[x, y, mod(z + 1, L)]
    result_z = Sz[mod(x - 1, L), y, z] + Sz[mod(x + 1, L), y, z] \
               + Sz[x, mod(y - 1, L), z] + Sz[x, mod(y + 1, L), z] \
               + Sz[x, y, mod(z - 1, L)] + Sz[x, y, mod(z + 1, L)]

    return array([result_x, result_y, result_z])


def threeD_single_spin_move(Sx, Sy, Sz, Tem):
    x = int(L * random.random())
    y = int(L * random.random())
    z = int(L * random.random())


    th = (-1) ** random.randint(1, 2) * arccos(random.uniform(1, -1))
    phi = (-1) ** random.randint(1, 2) * (random.random() - 0.5) * 2 * pi

    d_Sx = sin(th) * cos(phi)
    d_Sy = sin(th) * sin(phi)
    d_Sz = cos(th)

    Sxprime = Sx[x, y, z] + d_Sx
    Syprime = Sy[x, y, z] + d_Sy
    Szprime = Sz[x, y, z] + d_Sz

    factor = (Sxprime**2 + Syprime**2 + Szprime**2)**(-1/2)
    Sxprime *= factor
    Syprime *= factor
    Szprime *= factor

    d_S = array([Sxprime-Sx[x, y, z],Syprime-Sy[x, y, z],Szprime-Sz[x, y, z]])

    E_difference = -J * d_S.dot(nn_sum(Sx, Sy, Sz, x, y, z))
    p = exp((- E_difference) / (k * Tem))

    if p >= random.random():
        Sx[x, y, z] = Sxprime
        Sy[x, y, z] = Syprime
        Sz[x, y, z] = Szprime
    else:
        pass
    return Sx, Sy, Sz


Sx = zeros([L, L, L])
Sy = zeros([L, L, L])
Sz = np.ones([L, L, L])

N_tem_step = 30
N_thermalizing = 5000
Nsample = 1000
Nsubsweep = 3 * N

# file1 = open(r"C:\Users\a1896\OneDrive\Desktop\Comp_stat_phys\csp\ex05\3D_Heisenberg_cubic_raw\N=" + str(L) + ".txt",
#              "w+")

file2 = open(r"C:\Users\a1896\OneDrive\Desktop\Comp_stat_phys\csp\ex05\3D_Heisenberg_cubic_raw\N=" + str(L) + "_Binder_1.txt",
             "w+")

for Tem in linspace(3.1, 0.1, N_tem_step):

    for i in tqdm(range(N_thermalizing)):
        Sx, Sy, Sz = threeD_single_spin_move(Sx, Sy, Sz, Tem)

    M_data = zeros(Nsample)
    E_data = zeros(Nsample)

    M2_data = zeros(Nsample)
    M4_data = zeros(Nsample)

    M_data[0] = avg_over_sites(Sx, Sy, Sz)[1]
    E_data[0] = avg_over_sites(Sx, Sy, Sz)[0]

    M2_data[0] = avg_over_sites(Sx, Sy, Sz)[1] ** 2
    M4_data[0] = avg_over_sites(Sx, Sy, Sz)[1] ** 4

    for n in tqdm(range(1, Nsample)):
        for _ in range(Nsubsweep):
            Sx, Sy, Sz = threeD_single_spin_move(Sx, Sy, Sz, Tem)
        M_data[n] = avg_over_sites(Sx, Sy, Sz)[1]
        E_data[n] = avg_over_sites(Sx, Sy, Sz)[0]

        M2_data[n] = avg_over_sites(Sx, Sy, Sz)[1] ** 2
        M4_data[n] = avg_over_sites(Sx, Sy, Sz)[1] ** 4

    M2_avg = np.mean(M2_data)
    M4_avg = np.mean(M4_data)
    M_avg = np.mean(M_data)
    M_std = np.std(M_data)
    E_avg = np.mean(E_data)
    E_std = np.std(E_data)
    Cv = N * (E_std ** 2) / (k * Tem ** 2)
    Kai = N * (M_std ** 2) / (k * Tem)

#     file1.write(str(Tem) + " " + str(E_avg) + " " + str(Cv) + " " + str(M_avg) + " " + str(Kai) + "\n")
    file2.write(str(Tem) + " " + str(M2_avg**2) + " " + str(M4_avg) + "\n")
#
# file1.close()
file2.close()

# =======================================================================================================================
# =======================================================================================================================
# =======================================================================================================================

""""""""""
READ TXT FILE
"""""""""
filename = r"C:\Users\a1896\OneDrive\Desktop\Comp_stat_phys\csp\ex05\3D_Heisenberg_cubic_raw\N=" + str(L) + ".txt"
T = []
E = []
Cv = []
Mag = []
M_sus = []
with open(filename, 'r') as file_to_read:
    data = file_to_read.readlines()
    for line in data:
        Tt, Energy, Specific_heat, Mz, Mz_sus = [float(i) for i in line.split()]
        T.append(Tt)
        E.append(Energy)
        Cv.append(Specific_heat)
        Mag.append(Mz)
        M_sus.append(Mz_sus)

plt.xlabel('Temperature', fontsize=11)
plt.ylabel('Thermal quantities for a' + str(L) + '*' + str(L) + '*' + str(L) + ' Ising cubic system', fontsize=11)
# my_xticks = arange(0, 5.5, 0.5)
# my_yticks = arange(-2,2.5,0.5)
# xticks(my_xticks, fontsize = 9)
# yticks(my_yticks ,fontsize = 9)

plt.plot(T, E, color='r', marker='^', fillstyle='none', linestyle='none', label='MC Energy')
plt.plot(T, Cv, color='g', marker='^', fillstyle='none', linestyle='none', label='MC Specific Heat')
# plt.plot(T, Mag, color='b', marker='.', fillstyle='none', linestyle='none', label='MC Magnetization')
# plt.plot(T, M_sus, color='k', marker='.', fillstyle='none', linestyle='none', label='MC Susceptibility')

plt.xlim(0, 3.2)
# plt.ylim(-3.1, 2)
# legend(loc = 'best')
plt.show()
