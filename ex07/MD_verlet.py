import os
import vtktools
import numpy as np
import matplotlib.pyplot as plt
from copy import deepcopy
from tqdm import tqdm

data_dir = r"C:\Users\a1896\OneDrive\Desktop\Comp_stat_phys\csp\ex07\MD_in_cubic_box_raw\simu"
if not os.path.exists(data_dir):
    os.makedirs(data_dir)


def r_rel_pbc(ri, rj):
    """
    Compute the relative position of particles i and j in a box
    with periodic boundary conditions.
    
    Args:
        ri: Position vector of particle i
        rj: Position vector of particle j (i must not be equal to j)
    
    Returns: 
        the shortest relative distance with correct orientation
        between particles i and j in periodic boundary conditions (PBC)
    """
    r_vec = ri - rj  # relative a vector pointing from j to i

    for k in range(3):
        r_k = ri[k] - rj[k]
        if abs(r_k) > L / 2.0:
            r_vec[k] = -np.sign(r_k) * (L - abs(r_k))

    return r_vec


def potential(ri, rj):
    """
    Args:
        ri: Position vector of particle i
        rj: Position vector of particle j (i must not be equal to j)

    Returns:
        (Lennard-Jones) potential energy between particles
    """
    # TODO: Compute the Lennard-Jones potential energy between particles i and j.
    # Hint: Take PBC and the distance cut-off into account.
    r_vec = r_rel_pbc(ri, rj)
    r = np.sqrt(r_vec[0] ** 2 + r_vec[1] ** 2 + r_vec[2] ** 2)
    V = 4 * eplison * ((sigma / r) ** 12 - (sigma / r) ** 6)

    return V


def force(ri, rj):
    """
    Args:
        ri: Position vector of particle i
        rj: Position vector of particle j (i must not be equal to j)

    Returns:
        (Lennard-Jones) force vector
    """
    # TODO: Compute the force vector due to (a pair of) Lennard-Jones potential energy.
    # Hint: Take PBC and the distance cut-off into account.
    r_vec = r_rel_pbc(ri, rj)
    r = np.sqrt(r_vec[0] ** 2 + r_vec[1] ** 2 + r_vec[2] ** 2)

    r_normal_vec = r_vec / r  # from j pointing to i

    F = 24 * eplison * (2 * (1 / r) ** 13 - (1 / r) ** 7)  # it is along the r_vec's direction
    F_vec = F * r_normal_vec

    return F_vec


def table_neighbors(r_current):
    """
    :param r_current:
    :return: return a list containing all neighbors (r<rc) of the corresponding particle
    """
    total_n_list = []
    for i in range(N):
        ri = r_current[i, :]
        indiv_n_list = []
        for j in range(N):
            rj = r_current[j, :]
            if j != i:
                r = np.sqrt(r_rel_pbc(ri, rj)[0] ** 2 + r_rel_pbc(ri, rj)[1] ** 2 + r_rel_pbc(ri, rj)[2] ** 2)
                if r < rc:
                    indiv_n_list.append(rj)
        total_n_list.append(indiv_n_list)

    return total_n_list


def energy(r_current, v_current):
    """
    Args:
        r_current: Current particle positions of the whole sys
        v_current: Current particle velocities of the whole sys

    Returns:
        Total energy of the whole system
    """
    # TODO: Compute the total kinetic energy (`E_kin`) of the system of particles.
    # mass is set to be 1 s.t. force=acceleration
    E_kin = 0
    for i in range(N):
        E_kin += 1 / 2 * (v_current[i, 0] ** 2 + v_current[i, 1] ** 2 + v_current[i, 2] ** 2)

    # TODO: Compute the total potential energy (`E_pot`) of the system.
    # Hint: Don't forget to take the distance cut-off `rc` into account.
    # Hint: Avoid double counting. have to divide it by 2
    neighbor_list = table_neighbors(r_current)
    E_pot = 0
    for i in range(N):
        ri = r_current[i, :]
        for n in range(len(neighbor_list[i])):
            rj = neighbor_list[i][n]
            E_pot += 1 / 2 * potential(ri, rj)

    return E_kin + E_pot


def stepVerlet(r_previous, r_current):
    """
    Args:
        r_previous: Particle positions at time t-dt
        r_current: Particle positions at time t

    Returns:
        Updated positions as well as velocities and forces according to the
        Verlet scheme
    """

    # if the Verlet step drifts the particle outside the box 
    #  restore the particle into the box according to PBC
    r_current_pbc = r_current % L

    # TODO: compute the total force (=acceleration) acting on each particle
    # since all mass set to be 1, the force=acceleration
    neighbor_list = table_neighbors(r_current_pbc)
    F = np.zeros((N, 3))  # the force array
    for i in range(N):
        ri = r_current_pbc[i, :]
        for n in range(len(neighbor_list[i])):
            rj = neighbor_list[i][n]
            F[i, :] += force(ri, rj)

    r_next = np.zeros((N, 3))  # positions after the Verlet step
    del_r = np.zeros((N, 3))  # position changes between two Verlet steps

    # TODO: compute the new positions using the Verlet scheme
    for i in range(N):
        #  TODO: compute r_next[i, :]
        r_next[i, :] = 2 * r_current_pbc[i, :] - r_previous[i, :] + dt ** 2 * F[i, :]
        del_r[i, :] = r_next[i, :] - r_previous[i, :]

        if any(r_current[i, :] != r_current_pbc[i, :]):
            # TODO: check if particle i went across the boundary in the previous time step
            r_current[i, :] = r_current_pbc[i, :]

    # TODO: compute the current particle velocities (`v_current`) using `del_r`
    v_current = del_r / dt / 2
    return r_current, v_current, r_next, F


"""
Parameters
"""
N = 30  # particle number
sigma = 1
eplison = 1
L = 10  # box length
rc = 2.5 * sigma  # cutoff-length
T = 100  # simulation steps
dt = 10 ** -3  # time step

"""
Initialization
"""
energy_arr = np.zeros(T)

#  TODO: generate a random initial condition for positions stored in the array `r_current`
# by sampling `N` particles inside the cubic box of volume L**3, centred at (L,L,L)/2.
r_current = L / 2 + (np.random.rand(N, 3) - 0.5) * L

#  TODO: sample initial velocity array `v_current` containing velocities of
#  `N` particles from a Gaussian distribution.
mu, sigma = 0, 1
v_x = np.random.normal(mu, sigma, N)
v_y = np.random.normal(mu, sigma, N)
v_z = np.random.normal(mu, sigma, N)
v_current = np.array([v_x, v_y, v_z]).T

E_kin = 0
for i in range(N):
    E_kin += 1 / 2 * (v_current[i, 0] ** 2 + v_current[i, 1] ** 2 + v_current[i, 2] ** 2)
# print(E_kin)
# print('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')


r_next = r_current + v_current * dt

# Run the time evolution for `T` steps:
vtk_writer = vtktools.VTK_XML_Serial_Unstructured()
for t in tqdm(range(T)):
    r_current, v_current, r_next, F_ij = stepVerlet(r_current, r_next)

    energy_arr[t] = energy(r_next % L, v_current)  # we input the r array with PBC
    print(v_current)
    r_current = r_current % L
    r_x = r_current[:, 0]
    r_y = r_current[:, 1]
    r_z = r_current[:, 2]
    F_x = F_ij[:, 0]
    F_y = F_ij[:, 1]
    F_z = F_ij[:, 2]
    vtk_writer.snapshot(os.path.join(data_dir, "MD" + str(t) + ".vtu"), r_x, r_y, r_z, x_force=F_x, y_force=F_y,
                        z_force=F_z)

vtk_writer.writePVD(os.path.join(data_dir, "MD.pvd"))

"""
Plotting the system energy
"""
plt.figure()
plt.plot(energy_arr)
plt.ylim(0, 1.1 * np.max(energy_arr))
plt.xlabel('Timesteps')
plt.ylabel('Energy')
plt.show()
