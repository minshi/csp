import matplotlib.pyplot as plt
import numpy as np
import matplotlib.cm as cm
import random
from tqdm.notebook import tqdm

"""
1d case
this is an inelastic system which will finally freeze
"""


def d_new(d_at_t, V_n_loc):
    return d_at_t + dt * V_n_loc


def main(r_initial, v_initial):
    """
    :param r_initial: position array of the particles, N x 1
    :param v_initial: velocity array of the particles, N x 1
    :param d_initial: relative distance array of the particles, N x N
    :return:
    """
    # check if any pair of particles collide
    d_initial = np.ones([N, N])
    for i in range(N):
        for j in range(N):
            if j != i:
                if i < j:
                    d_initial[i, j] = np.abs(r_initial[i] - r_initial[j]) - 2 * R_ball
    # calculate the relative velocity between each pair
    V_n_loc_free = np.zeros([N, N])
    d_v_array = np.zeros([N, 1])
    for i in range(N-1):
        """
        In the for loop, we only record the velocity update for each pair of colliding particles 
        After finishing the for loop, we update the positions of the whole system
        """
        j = (i+1) % N # in 1-d only nearest neighbours can collide
        V_n_loc_free[i, j] = -(v_initial[i] - v_initial[j])
        d_new_t = d_new(d_initial, V_n_loc_free)

        if d_new_t[i, j] <= 0:
            m_eff = m / 2
            V_n_loc = - d_initial[i, j] / dt
            R_n_loc = m_eff * (V_n_loc - V_n_loc_free[i, j]) / dt
            H_vec = np.array([-1, 1]).T
            R_vec = H_vec * R_n_loc
            d_v_array[i] += dt * R_vec[0] / m
            d_v_array[j] += dt * R_vec[1] / m

    # update the velocity and position of the whole system at the next time step
    v_initial += d_v_array
    r_initial += dt * v_initial

    # check if any particle of the system is out of the boundary
    # consider purely elastic collision with the wall
    for i in range(N):
        if r_initial[i] + R_ball > L or r_initial[i] - R_ball < 0:
            v_initial[i] *= -1
    return r_initial, v_initial


N = 10
L = 100
R_ball = 1
m = 1
v_starting = np.random.normal(0, 40, (N, 1)) % L
dt = R_ball / np.max(v_starting) /2
# manually set the initial condition to avoid overlap at the very beginning
r_starting = np.zeros([N, 1])
for i in range(N):
    L_min = i / N * L
    L_max = (i + 1) / N * L
    r_starting[i] = np.random.uniform(L_min, L_max)



n_steps = 1000000
colors = np.linspace(0, 1, N)
cmap = cm.get_cmap('Spectral')

for i in range(n_steps):
    plt.clf()
    plt.scatter(r_starting, np.zeros(N), marker='o', c=cm.jet(colors))
    plt.xlim(0, 100)
    plt.pause(10e-4)
    r_starting, v_starting = main(r_starting, v_starting)
