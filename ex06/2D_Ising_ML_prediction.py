import numpy as np
from tensorflow import keras
L = 30
batch_size = 1000


"""
========================================================================================================================
Make prediction
========================================================================================================================
"""
model = keras.models.load_model(r'C:\Users\a1896\OneDrive\Desktop\Comp_stat_phys\csp\ex06\2D_Ising_square_sample\2D_Ising_square_Keras')
"""
label the T>Tc config with 1
"""
#it is 5 * 2 * 30*30 (5 diff Tems, 10 configs for each Tem, the array size for the system is 30*30) so total dim is 1500*30
spin_config_above_Tc_check = np.loadtxt(r"C:\Users\a1896\OneDrive\Desktop\Comp_stat_phys\csp\ex06\2D_Ising_square_sample\N="
                                     + str(L) + "_spin_config_above_Tc_check_2.txt")
x_check1 = np.zeros([batch_size, L, L])
for i in range(batch_size):
    j = i*4
    x_check1[i, :,:] = spin_config_above_Tc_check[j:j+L,:]
y_check1 = np.ones(batch_size).T


"""
label the T>Tc config with 0
"""
spin_config_below_Tc_check = np.loadtxt(r"C:\Users\a1896\OneDrive\Desktop\Comp_stat_phys\csp\ex06\2D_Ising_square_sample\N="
                                     + str(L) + "_spin_config_below_Tc_check_2.txt")
x_check0 = np.zeros([batch_size, L, L])
for i in range(batch_size):
    j = i*4
    x_check0[i, :,:] = spin_config_below_Tc_check[j:j+L,:]
y_check0 = np.zeros(batch_size).T



# batch size is 30, input size is LxL
x_check_tot = np.concatenate((x_check1, x_check0), axis=0)
#print(x_check_tot)
y_check_tot = np.concatenate((y_check1, y_check0))
# make probability predictions with the model
predictions = (model.predict(x_check_tot) > 0.5).astype(int).reshape(-1)
# round predictions
y_check_tot = y_check_tot.astype(int)
a = np.array(y_check_tot-predictions.T)
accuracy = (np.bincount(abs(a))[0])/len(a)
print(accuracy)



# The reconstructed model is already compiled and has retained the optimizer
# state, so training can resume:
#reconstructed_model.fit(test_input, test_target)

