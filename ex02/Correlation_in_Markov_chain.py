from numpy import ones,copy, exp, mod, zeros, savetxt, array, sum,vstack
import random
random.seed()


"""
3D Ising Cubic
"""
L = 4
d = 3
k = 1
J = 1
def threeD_single_spin_flip(state, E_initial, M_initial, Tem):
    test = state
    x = int(L * random.random())
    y = int(L * random.random())
    z = int(L * random.random())
    E_difference = 2 * J * test[x, y, z] * (test[mod(x - 1, L), y, z] + test[mod(x + 1, L), y, z]
                                            + test[x, mod(y - 1, L), z] + test[x, mod(y + 1, L), z]
                                            + test[x, y, mod(z - 1, L)] + test[x, y, mod(z + 1, L)])
    p = exp((- E_difference) / (k * Tem))
    if p >= random.random():
        state[x, y, z] *= -1
        E_initial += E_difference
        if state[x, y, z] < 0:
            M_initial -= 2
        if state[x, y, z] > 0:
            M_initial += 2
    else:
        pass
    return state, E_initial, M_initial


def thermalizing_stage(Start_state,Start_E, Start_M, Tem, T_examine):
    thermo_steps = 10000 * L ** d
    sampling_number = 1000 # number of copies of the sample
    M_in_time = zeros([thermo_steps, sampling_number])

    initial_state = copy(Start_state)
    E_initial = copy(Start_E)
    M_initial = copy(Start_M)

    for k in range(len(T_examine)):
        if abs(Tem - T_examine[k]) < 0.05:

            for i in range(sampling_number):

                initial_state = copy(Start_state)
                E_initial = copy(Start_E)
                M_initial = copy(Start_M)

                for j in range(thermo_steps):
                    M_in_time[j, i] = M_initial
                    new_state, new_E, new_M = threeD_single_spin_flip(initial_state, E_initial, M_initial, Tem)
                    initial_state = new_state
                    E_initial = new_E
                    M_initial = new_M
            M_thermalizing_in_time = sum(M_in_time, axis = 1) / sampling_number / L ** d
            # Nonlinear correlation function at the thermalzing stage
            savetxt(r"C:\Users\a1896\OneDrive\Desktop\Comp_stat_phys\csp\ex02\3D_Ising_cubic_raw\N=" + str(L) + \
           "_Nonlinear_correlation_T=" + str(T_examine[k]) + ".txt", M_thermalizing_in_time)

    return initial_state, E_initial, M_initial



def averaging_stage(start_state, start_E, start_M, Tem, T_examine):
    E_avg = 0.0
    Decorrelate_steps = 1000 * L ** d
    sampling_number = 1000
    M_in_time = zeros([Decorrelate_steps, sampling_number])
    M2_in_time = zeros([Decorrelate_steps, sampling_number])

    for k in range(len(T_examine)):
        if abs(Tem - T_examine[k]) < 0.05:

            for x in range(sampling_number):

                filtered_state = copy(start_state)
                filtered_E = copy(start_E)
                filtered_M = copy(start_M)

                for y in range(Decorrelate_steps):
                    M_in_time[y, x] = filtered_M
                    M2_in_time[y, x] = sum(filtered_state*start_state)
                    new_state, new_E, new_M = threeD_single_spin_flip(filtered_state, filtered_E, filtered_M, Tem)
                    filtered_state = new_state
                    filtered_E = new_E
                    filtered_M = new_M

            M_averging_in_time = array(sum(M_in_time, axis=1) / sampling_number / L ** d)
            M_0_and_t = array(sum(M2_in_time, axis=1) / sampling_number / L ** d)
            M_data = vstack((M_averging_in_time , M_0_and_t)).T
            # Linear correlation function at the averaging stage
            savetxt(r"C:\Users\a1896\OneDrive\Desktop\Comp_stat_phys\csp\ex02\3D_Ising_cubic_raw\N=" + str(L) + \
                    "_Linear_correlation_T=" + str(T_examine[k]) + ".txt", M_data)
    return E_avg



state_initial = -ones([L, L, L], int)
E_initial = -3 * J * L ** d
M_initial = -L ** d # since we start with the simple all down initial state
T_examine = [6.5, 4.2, 3.7, 2.0]
for Tem in T_examine:
    filtered_state, filtered_E, filtered_M = thermalizing_stage(state_initial, E_initial, M_initial, Tem, T_examine)
    g = averaging_stage(filtered_state, filtered_E, filtered_M, Tem, T_examine)


