import os
import vtktools
import numpy as np
import matplotlib.pyplot as plt
from copy import deepcopy
from tqdm import tqdm
import random

random.seed()

data_dir = r"C:\Users\a1896\OneDrive\Desktop\Comp_stat_phys\csp\ex10\MD_canonical_raw\simu"
if not os.path.exists(data_dir):
    os.makedirs(data_dir)


def r_rel_pbc(ri, rj):
    """
    Compute the relative position of particles i and j in a box
    with periodic boundary conditions.
    
    Args:
        ri: Position vector of particle i
        rj: Position vector of particle j (i must not be equal to j)
    
    Returns: 
        the shortest relative distance with correct orientation
        between particles i and j in periodic boundary conditions (PBC)
    """
    r_vec = ri - rj  # relative distance without PBC

    for k in range(3):
        r_k = ri[k] - rj[k]
        if abs(r_k) > L / 2.0:
            r_vec[k] = -np.sign(r_k) * (L - abs(r_k))

    return r_vec


def potential(ri, rj):
    """
    Args:
        ri: Position vector of particle i
        rj: Position vector of particle j (i must not be equal to j)

    Returns:
        (Lennard-Jones) potential energy between particles
    """
    r_vec = r_rel_pbc(ri, rj)
    r = np.linalg.norm(r_vec)

    if r > rc:  # cut-off
        return 4 * ((1 / rc) ** 12 - (1 / rc) ** 6)
    else:
        return 4 * ((1 / r) ** 12 - (1 / r) ** 6)


def force(ri, rj):
    """
    Args:
        ri: Position vector of particle i
        rj: Position vector of particle j (i must not be equal to j)

    Returns:
        (Lennard-Jones) force vector
    """
    r_vec = r_rel_pbc(ri, rj)
    r = np.linalg.norm(r_vec)

    if r > rc:  # cut-off
        return np.zeros(3)
    else:
        return 24 * (2 * (1 / r) ** 14 - (1 / r) ** 8) * r_vec


def xi_a(v_current, xi):
    Q = 10
    d_xi = 1 / Q * (np.sum(v_current ** 2) - (3 * N + 1) * T)
    #print(d_xi)
    xi_new = xi + d_xi * dt
    return xi_new


def energy(r_current, v_current):
    """
    Args:
        r_current: Current particle positions
        v_current: Current particle velocities

    Returns:
        Total energy of the system
    """
    E_kin = 0.5 * sum(sum(np.square(v_current)))

    E_pot = 0
    V_c = 4 * ((1 / rc) ** 12 - (1 / rc) ** 6)  # cutoff potential

    for i in range(len(r_current)):
        for j in range(len(r_current) - i - 1):
            E_pot += potential(r_current[i, :], r_current[i + j + 1, :]) - V_c

    return E_kin + E_pot


def stepVerlet(r_previous, r_current, v_current, xi):
    """
    Args:
        r_previous: Particle positions at time t-dt
        r_current: Particle positions at time t

    Returns:
        Updated positions as well as velocities and forces according to the
        Verlet scheme
    """
    # compute the forces acting on every particle (=acceleration)

    r_current_pbc = r_current % L

    F = np.zeros((N, 3))
    xi_new = xi_a(v_current, xi)


    for i in range(N):
        for j in range(N):
            if i != j:
                # add force from pairing interactions with other particles
                F[i, :] += force(r_current_pbc[i, :], r_current_pbc[j, :])
        # add force due to friction with the thermostat
        F[i, :] -= xi_new * v_current[i, :]

    r_next = np.zeros((N, 3))
    del_r = np.zeros((N, 3))

    # compute the new positions using the Verlet scheme
    for i in range(N):
        r_next[i, :] = 2 * r_current[i, :] - r_previous[i, :] + F[i, :] * dt ** 2
        del_r[i, :] = r_next[i, :] - r_previous[i, :]

        if any(r_current[i, :] != r_current_pbc[i, :]):
            # particle i went across the boundary in the previous time step
            r_current[i, :] = r_current_pbc[i, :]
            r_next[i, :] = r_next[i, :] % L

    v_current = del_r / (2 * dt)

    return r_current, v_current, r_next, F, xi_new


"""
Parameters
"""
N = 30  # particle number
L = 10  # box length
rc = 2.5  # cutoff-length
t_step = 5000  # simulation steps
dt = 10 ** -3  # time step
M = int(L / rc)  # number of cells in one direction

"""
Initialization
"""
r_current = np.zeros((N, 3))
cell_index = np.array([[i, j, k] for i in range(M) for j in range(M) for k in range(M)])
for i in range(N):
    cell_n = random.randint(0, len(cell_index[:, 0]) - 1)
    r_current[i, :] = cell_index[cell_n, :] * rc + np.random.rand(3) * rc
    cell_index = np.delete(cell_index, cell_n, axis=0)
v_current = np.random.normal(0, 10, (N, 3))  # particle velocities at time t0
r_next = r_current + v_current * dt  # particle positions at time t0+dt

# set the instantaneous temperature
tau = 2 / (3 * (N - 1)) * np.sum(v_current ** 2 / 2)
# set the desired temperature
T = tau + 10
# generate the initial friction coefficient
xi_0 = 0.1 * (1 - T / tau)
print(tau, xi_0)
"""
Simulation
"""
energy_arr = np.zeros(t_step)
tem_arr = np.zeros(t_step)
vtk_writer = vtktools.VTK_XML_Serial_Unstructured()
for t in tqdm(range(t_step)):
    r_current, v_current, r_next, F_ij, xi_0 = stepVerlet(r_current, r_next, v_current, xi_0)
    tem_arr[t] = 1 / (3 * (N - 1)) * np.sum(v_current ** 2)
    energy_arr[t] = energy(r_next % L, v_current)

    r_current = r_current % L
    r_x = r_current[:, 0]
    r_y = r_current[:, 1]
    r_z = r_current[:, 2]
    F_x = F_ij[:, 0]
    F_y = F_ij[:, 1]
    F_z = F_ij[:, 2]
    vtk_writer.snapshot(os.path.join(data_dir, "MD" + str(t) + ".vtu"), r_x, r_y, r_z,
                        x_force=F_x, y_force=F_y, z_force=F_z)

vtk_writer.writePVD(os.path.join(data_dir, "MD.pvd"))

"""
Plotting the system energy
"""
plt.figure()
plt.plot(energy_arr)
#plt.xlim(0,178)
# plt.ylim(0.9*np.min(energy_arr), 1.1*np.max(energy_arr))
plt.xlabel('Timesteps')
plt.ylabel('Energy')
plt.show()

plt.figure()
plt.plot(tem_arr)
#plt.xlim(0,178)
# plt.ylim(0.9*np.min(energy_arr), 1.1*np.max(energy_arr))
plt.xlabel('Timesteps')
plt.ylabel('Temperature')
plt.show()
