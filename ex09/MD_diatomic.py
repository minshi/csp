import os
import vtktools
import numpy as np
import matplotlib.pyplot as plt
from copy import deepcopy
from tqdm import tqdm
import random
random.seed()

def r_rel_pbc(ri, rj):
    """
    Compute the relative position of particles i and j in a box
    with periodic boundary conditions.
    
    Args:
        ri: Position vector of particle i
        rj: Position vector of particle j (i must not be equal to j)
    
    Returns: 
        the shortest relative distance with correct orientation
        between particles i and j in periodic boundary conditions (PBC)
    """
    r_vec = ri - rj  # relative distance without PBC

    for k in range(3):
        r_k = ri[k] - rj[k]
        if abs(r_k) > L / 2.0:
            r_vec[k] = -np.sign(r_k) * (L - abs(r_k))

    return r_vec


def potential(ri, rj):
    """
    Args:
        ri: Position vector of particle i
        rj: Position vector of particle j (i must not be equal to j)

    Returns:
        (Lennard-Jones) potential energy between particles
    """
    r_vec = r_rel_pbc(ri, rj)
    r = np.linalg.norm(r_vec)

    if r > rc:  # cut-off
        return 4 * ((1 / rc) ** 12 - (1 / rc) ** 6)
    else:
        return 4 * ((1 / r) ** 12 - (1 / r) ** 6)


def force(ri, rj):
    """
    Args:
        ri: Position vector of particle i
        rj: Position vector of particle j (i must not be equal to j)

    Returns:
        (Lennard-Jones) force vector
    """
    r_vec = r_rel_pbc(ri, rj)
    r = np.linalg.norm(r_vec)

    if r > rc:  # cut-off
        return np.zeros(3)
    else:
        return 24 * (2 * (1 / r) ** 14 - (1 / r) ** 8) * r_vec


def potenergy(r_current1, r_current2):
    """
    Args:
        r_current: Current particle positions
        v_current: Current particle velocities

    Returns:
        Total energy of the system
    """

    E_pot = 0
    V_c = 4 * ((1 / rc) ** 12 - (1 / rc) ** 6)  # cutoff potential

    for i in range(N):
        for j in range(N):
            if i != j:
                E_pot += potential(r_current1[i, :], r_current1[j, :]) - V_c
                E_pot += potential(r_current1[i, :], r_current2[j, :]) - V_c
                E_pot += potential(r_current2[i, :], r_current2[j, :]) - V_c
                E_pot += potential(r_current2[i, :], r_current1[j, :]) - V_c

    return E_pot/2


def multiplier(r_t, r_tilde_new_t):
    """
    :param r_t:
    :param r_tilde_new_t:
    :return: Return the lambda_prime since we don't really divide by dt^2 here
    """

    # rr = np.dot(r_t, r_t)=d^2
    rr_tidle = np.dot(r_t, r_tilde_new_t)
    r_tilde_2 = np.dot(r_tilde_new_t, r_tilde_new_t)
    Delta = np.sqrt(rr_tidle ** 2 - d**2 * (r_tilde_2-d**2))

    numerate_1 = -rr_tidle + Delta
    numerate_2 = -rr_tidle - Delta

    if np.abs(numerate_1) <= np.abs(numerate_2):
        lam = numerate_1 / 2 / d ** 2
    else:
        lam = numerate_2 / 2 / d ** 2
    return lam


def stepVerlet(r_previous1, r_previous2, r_current1, r_current2):
    """
    Args:
        r_previous1: Particle positions of atom 1 at time t-dt
        r_current1: Particle positions of atom 1 at time t

    Returns:
        Updated positions as well as velocities and forces according to the
        Verlet scheme
    """
    # compute the forces acting on every particle (=acceleration)

    r_current_pbc1 = r_current1 % L
    r_current_pbc2 = r_current2 % L

    F1 = np.zeros((N, 3))
    F2 = np.zeros((N, 3))

    for i in range(N):
        for j in range(N):
            if i != j:
                F1[i, :] += force(r_current_pbc1[i, :], r_current_pbc1[j, :])
                F1[i, :] += force(r_current_pbc1[i, :], r_current_pbc2[j, :])
                F2[i, :] += force(r_current_pbc2[i, :], r_current_pbc2[j, :])
                F2[i, :] += force(r_current_pbc2[i, :], r_current_pbc1[j, :])

    r_next1 = np.zeros((N, 3))
    del_r1 = np.zeros((N, 3))
    r_next2 = np.zeros((N, 3))
    del_r2 = np.zeros((N, 3))

    # compute the new positions using the Verlet scheme with only intermolecular force
    for i in range(N):
        r_next1[i, :] = 2 * r_current1[i, :] - r_previous1[i, :] + F1[i, :] * dt ** 2
        r_next2[i, :] = 2 * r_current2[i, :] - r_previous2[i, :] + F2[i, :] * dt ** 2

    # modify the new positions according to intramolecular force
    for i in range(N):
        r_t = r_rel_pbc(r_current_pbc1[i, :], r_current_pbc2[i, :])
        r_tilde_new_t = r_rel_pbc(r_next1[i, :] % L,
                                  r_next2[i, :] % L)  # since the actual positions of r_next should be
        # inside the box and the effective potential between two particles take only the smallest distance (with PBC)
        # inbetween into account
        lam = multiplier(r_t, r_tilde_new_t)

        # since g_1 = - g_2
        # we can avoid multipling dt^2 here simply by not dividing dt^2 in the calculation of lambda
        r_next1[i, :] = r_next1[i, :] + lam * r_t
        r_next2[i, :] = r_next2[i, :] - lam * r_t

        del_r1[i, :] = r_next1[i, :] - r_previous1[i, :]
        del_r2[i, :] = r_next2[i, :] - r_previous2[i, :]

        if any(r_current1[i, :] != r_current_pbc1[i, :]):
            r_current1[i, :] = r_current_pbc1[i, :]
            r_next1[i, :] = r_next1[i, :] % L
        if any(r_current2[i, :] != r_current_pbc2[i, :]):
            r_current2[i, :] = r_current_pbc2[i, :]
            r_next2[i, :] = r_next2[i, :] % L

    v_current1 = del_r1 / (2 * dt)
    v_current2 = del_r2 / (2 * dt)

    return r_current1, r_current2, v_current1, v_current2, r_next1, r_next2, F1, F2


"""
Parameters
"""
N = 60  # MOLECULAR number, for which particle number is 2N
L = 10  # box length
d = 0.1  # intramolecular distance
rc = 2.5  # cutoff-length
T = 1000  # simulation steps
dt = 10 ** -3  # time step
M = int(L / rc)  # number of cells in one direction
data_dir = r"C:\Users\a1896\OneDrive\Desktop\Comp_stat_phys\csp\ex07\MD_in_cubic_box_raw\simu"
if not os.path.exists(data_dir):
    os.makedirs(data_dir)
"""
Initialization
"""
pot_energy_arr = np.zeros(T)
kin_energy_arr = np.zeros(T)
r_current1 = np.zeros((N, 3))  # particle positions at time t0

# Divergence problems with previous exercises indeed lies on the unphysical initial config!!
# To avoid the too-close distance or even overlap between 2 molecules to the best, we seem to be only allowed to
# initialize the config with each molecule assigned to different cells.
# [TO DO] Do we have other effective way to do it?
"""
[TO DO] But in this case, we can only have particle number <= cell number, i.e. sparse case, what if we want to simulate the 
dense case?
"""
cell_index = np.array([[i, j, k] for i in range(M) for j in range(M) for k in range(M)])
for i in range(N):
    cell_n = random.randint(0, len(cell_index[:, 0])-1)
    r_current1[i, :] = cell_index[cell_n, :] * rc + np.random.rand(3) * rc
    cell_index = np.delete(cell_index, cell_n, axis=0)

r_current2 = deepcopy(r_current1)
for i in range(N):
    comp = random.randint(0,2)
    r_current2[i, comp] = (r_current2[i, comp] - d) % L
#r_current2[:, 0] = (r_current2[:, 0] - d) % L  # randomize the position of 2nd particle in each molecule, it is not so
# good to choose an arbitrary axis to put the 2nd atom at distance = d since it may also be too close to the neighboring
# particles which may lead to energy divergence. So we can only choose on axis along which we put the 2nd atom at
# distance = d

v_current = np.random.normal(0, 5, (N, 3))  # particle velocities at time t0
# particle positions at time t0+dt
r_next1 = r_current1 + v_current * dt
r_next2 = r_current2 + v_current * dt
# [TO DO]this step may also lead to some too-close distance or overlap, which may potentially lead to divergence,
# how to deal with it

"""
Simulation starts
"""
vtk_writer1 = vtktools.VTK_XML_Serial_Unstructured()
vtk_writer2 = vtktools.VTK_XML_Serial_Unstructured()
for t in tqdm(range(T)):
    r_current1, r_current2, v_current1, v_current2, r_next1, r_next2, F_ij1, F_ij2 = stepVerlet(r_current1, r_current2,
                                                                                                r_next1, r_next2)

    pot_energy_arr[t] = potenergy(r_current1 % L, r_current2 % L)
    kin_energy_arr[t] = 0.5 *sum(sum(np.square(v_current1))) + 0.5 * sum(sum(np.square(v_current2)))

    r_x1 = r_current1[:, 0]
    r_y1 = r_current1[:, 1]
    r_z1 = r_current1[:, 2]
    r_x2 = r_current2[:, 0]
    r_y2 = r_current2[:, 1]
    r_z2 = r_current2[:, 2]
    F_x1 = F_ij1[:, 0]
    F_y1 = F_ij1[:, 1]
    F_z1 = F_ij1[:, 2]
    F_x2 = F_ij2[:, 0]
    F_y2 = F_ij2[:, 1]
    F_z2 = F_ij2[:, 2]
    vtk_writer1.snapshot(os.path.join(data_dir, "MD" + str(t) + "1.vtu"), r_x1, r_y1, r_z1, x_force=F_x1, y_force=F_y1,
                         z_force=F_z1)
    vtk_writer2.snapshot(os.path.join(data_dir, "MD" + str(t) + "2.vtu"), r_x2, r_y2, r_z2, x_force=F_x2, y_force=F_y2,
                         z_force=F_z2)

vtk_writer1.writePVD(os.path.join(data_dir, "MD1.pvd"))
vtk_writer1.writePVD(os.path.join(data_dir, "MD2.pvd"))

energy_arr = pot_energy_arr + kin_energy_arr

"""
Plotting the system energy
"""
plt.figure()
plt.plot(energy_arr)
# plt.ylim(0.9*np.min(energy_arr), 1.1*np.max(energy_arr))
plt.xlabel('Timesteps')
plt.ylabel('Total_Energy')


plt.figure()
plt.plot(pot_energy_arr)
# plt.ylim(0.9*np.min(energy_arr), 1.1*np.max(energy_arr))
plt.xlabel('Timesteps')
plt.ylabel('Potential_Energy')

plt.figure()
plt.plot(kin_energy_arr)
# plt.ylim(0.9*np.min(energy_arr), 1.1*np.max(energy_arr))
plt.xlabel('Timesteps')
plt.ylabel('Kinetic_Energy')

plt.show()
