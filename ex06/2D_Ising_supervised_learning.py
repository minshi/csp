import numpy as np
from tensorflow.keras import Sequential
from tensorflow.python.keras.layers import Dense, Flatten
import matplotlib.pyplot as plt
L = 30
batch_size = 10000
"""
label the T>Tc config with 1
"""
#it is 5 * 2 * 30*30 (5 diff Tems, 10 configs for each Tem, the array size for the system is 30*30) so total dim is 1500*30
spin_config_above_Tc_1 = np.loadtxt(r"C:\Users\a1896\OneDrive\Desktop\Comp_stat_phys\csp\ex06\2D_Ising_square_sample\N="
                                     + str(L) + "_spin_config_above_Tc.txt")
x_1 = np.zeros([batch_size, L, L])
for i in range(batch_size):
    j = i*4
    x_1[i, :,:] = spin_config_above_Tc_1[j:j+L,:]
y_1 = np.ones(batch_size).T


"""
label the T>Tc config with 0
"""
spin_config_below_Tc_0 = np.loadtxt(r"C:\Users\a1896\OneDrive\Desktop\Comp_stat_phys\csp\ex06\2D_Ising_square_sample\N="
                                     + str(L) + "_spin_config_below_Tc.txt")
x_0 = np.zeros([batch_size, L, L])
for i in range(batch_size):
    j = i*4
    x_0[i, :,:] = spin_config_below_Tc_0[j:j+L,:]
y_0 = np.zeros(batch_size).T

# split into input (X) and output (y) variables
# batch size is 20, input size is LxL
x_train = np.concatenate((x_1, x_0), axis=0)
y_train = np.concatenate((y_1, y_0))
print(x_train.shape,y_train.shape)


# We use 1 hidden layer, 100 neurons (sigmoid activation function), binary cross entropy cost function as loss function,
# Use Adam method for stochastic optimization
# Sigmoid function on the output layer can ensure our network output is between 0 and 1 and easy to map to either a probability
# of class 1 or snap to a hard classification of either class with a default threshold of 0.5.


# define the keras model
model = Sequential()
# the input dim here ask about the number of input value, i.e. number of columns, since the input values are
# just scalar with {-1, 1}, so the dim is just 1
model.add(Flatten(input_shape=(L, L)))
# We have to reshape the input data into 1d, but why?
# It does not really matter, although we lose the info of the data shape. Since it is a fully connected neurol network
# s.t. all points are interrelated w.r.t. each other. And finally the data will be reshpaed back as well.
model.add(Dense(100, input_dim=1, activation='sigmoid'))
model.add(Dense(1, activation='sigmoid'))
# compile the keras model
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

# fit the keras model on the dataset

history = model.fit(x_train, y_train, validation_split = 0.1, epochs = 150, batch_size = batch_size, verbose=0)
# evaluate the keras model
loss, accuracy = model.evaluate(x_train, y_train)
print('Accuracy: %.2f' % (accuracy*100))

model.save(r"C:\Users\a1896\OneDrive\Desktop\Comp_stat_phys\csp\ex06\2D_Ising_square_sample\2D_Ising_square_Keras")

plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
# validation_split let you to set some of the input in the batch in each epoch (each ML round) as the test_set
# instead of the train_set (in order to let the modified ML check if the prediction for this test_set is good or not).
# So the train is good if both of the accuracy are good (if val_accuracy is bad, it may mean that it is over-fitting)
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'val'], loc='upper left')
plt.show()


plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'val'], loc='upper left')
plt.show()


