import matplotlib.pyplot as plt
from numpy import linspace, exp, mod, zeros, pi, arccos, cos, sin, array
import numpy as np
import random
from tqdm import tqdm

random.seed()

"""
3D Ising Cubic
"""
L = 4
d = 3
N = L ** d
k = 1
J = 1


def avg_over_sites(S):
    E = 0
    for i in range(L):
        for j in range(L):
            for k in range(L):
                E += -J * (S[i, j, k] * (S[mod(i + 1, L), j, k] + S[i, mod(j + 1, L), k] + S[i, j, mod(k + 1, L)]))
    M = np.sum(S)
    return E/N , M/N


def nn_sum(S, x, y, z):
    result = S[mod(x - 1, L), y, z] + S[mod(x + 1, L), y, z] \
               + S[x, mod(y - 1, L), z] + S[x, mod(y + 1, L), z] \
               + S[x, y, mod(z - 1, L)] + S[x, y, mod(z + 1, L)]
    return result


"""
Metropolis single spin flip algorithm
"""
# def threeD_single_spin_move(S, Tem):
#     x = int(L * random.random())
#     y = int(L * random.random())
#     z = int(L * random.random())
#
#     d_S = -2* S[x, y, z]
#
#     E_difference = -J * d_S*nn_sum(S, x, y, z)
#     p = exp((- E_difference) / (k * Tem))
#
#     if p >= random.random():
#         S[x, y, z] = S[x, y, z] + d_S
#     else:
#         pass
#     return S


"""
Cluster-flipping-Wolff algorithm
"""
def neighbor_spin(index, S, i ,j, k):
    if index == -1:
        # check left--X axis
        Sj = S[mod(i-1, L), j, k]
        if S[i, j, k] * Sj > 0:
            return mod(i - 1, L), j, k, 0
        else:
            return 1, 1, 1, 1
    if index == 1:
        Sj = S[mod(i+1, L), j, k]
        if S[i, j, k] * Sj > 0:
            return mod(i + 1, L), j, k, 0
        else:
            return 1, 1, 1, 1
    if index == -2:
        # check behind--Y axis
        Sj = S[i, mod(j - 1, L), k]
        if S[i, j, k] * Sj > 0:
            return i, mod(j-1, L), k, 0
        else:
            return 1, 1, 1, 1
    if index == 2:
        Sj = S[i, mod(j + 1, L), k]
        if S[i, j, k] * Sj > 0:
            return i, mod(j + 1, L), k, 0
        else:
            return 1, 1, 1, 1
    if index == -3:
        # check underneath--Z axis
        Sj = S[i, j, mod(k-1, L)]
        if S[i, j, k] * Sj > 0:
            return i, j, mod(k-1, L), 0
        else:
            return 1, 1, 1, 1
    if index == 3:
        Sj = S[i, j, mod(k + 1, L)]
        if S[i, j, k] * Sj > 0:
            return i, j, mod(k + 1, L), 0
        else:
            return 1, 1, 1, 1


def cluster_flipping(S, Tem):
    cluster = []
    check = []
    delete_list = []
    i = int(L * random.random())
    j = int(L * random.random())
    k = int(L * random.random())
    cluster.append([i, j, k])
    check.append([i, j, k])

    while len(check) != 0:
        index = check.pop(0)
        for nn_index in [-3, -2, -1, 1, 2, 3]:
            i_prime, j_prime, k_prime, bond_index = neighbor_spin(nn_index, S, index[0], index[1], index[2])
            p_bond_forming = 1 - np.exp(-2/Tem*J)
            if bond_index == 0:
                if p_bond_forming >= random.random():
                    exist_count = cluster.count([i_prime, j_prime, k_prime])
                    if exist_count == 0:
                        check.append([i_prime, j_prime, k_prime])
                        cluster.append([i_prime, j_prime, k_prime])

    for t in range(len(cluster)):
        S[cluster[t][0], cluster[t][1], cluster[t][2]] *= -1

    return S



S = np.ones([L, L, L])
N_tem_step = 10
N_thermalizing = 1000
Nsample = 100
Nsubsweep = 3 * N

file1 = open(r"C:\Users\a1896\OneDrive\Desktop\Comp_stat_phys\csp\ex05\3D_Ising_cubic_raw\N=" + str(L) + "_cluster.txt",
             "w+")

for Tem in linspace(6.1, 0.1, N_tem_step):

    for i in tqdm(range(N_thermalizing)):
        S = cluster_flipping(S, Tem)
        #S = threeD_single_spin_move(S, Tem)

    M_data = zeros(Nsample)
    E_data = zeros(Nsample)

    M2_data = zeros(Nsample)
    M4_data = zeros(Nsample)

    M_data[0] = avg_over_sites(S)[1]
    E_data[0] = avg_over_sites(S)[0]

    M2_data[0] = avg_over_sites(S)[1] ** 2
    M4_data[0] = avg_over_sites(S)[1] ** 4

    for n in tqdm(range(1, Nsample)):
        for _ in range(Nsubsweep):
            S = cluster_flipping(S, Tem)
            #S = threeD_single_spin_move(S, Tem)
        M_data[n] = avg_over_sites(S)[1]
        E_data[n] = avg_over_sites(S)[0]

        M2_data[n] = avg_over_sites(S)[1] ** 2
        M4_data[n] = avg_over_sites(S)[1] ** 4

    M2_avg = np.mean(M2_data)
    M4_avg = np.mean(M4_data)
    M_avg = np.mean(M_data)
    M_std = np.std(M_data)
    E_avg = np.mean(E_data)
    E_std = np.std(E_data)
    Cv = N * (E_std ** 2) / (k * Tem ** 2)
    Kai = N * (M_std ** 2) / (k * Tem)

    file1.write(str(Tem) + " " + str(E_avg) + " " + str(Cv) + " " + str(M_avg) + " " + str(Kai) + "\n")

file1.close()


# =======================================================================================================================
# =======================================================================================================================
# =======================================================================================================================

""""""""""
READ TXT FILE
"""""""""
filename = r"C:\Users\a1896\OneDrive\Desktop\Comp_stat_phys\csp\ex05\3D_Ising_cubic_raw\N=" + str(L) + "_cluster.txt"
T = []
E = []
Cv = []
Mag = []
M_sus = []
with open(filename, 'r') as file_to_read:
    data = file_to_read.readlines()
    for line in data:
        Tt, Energy, Specific_heat, Mz, Mz_sus = [float(i) for i in line.split()]
        T.append(Tt)
        E.append(Energy)
        Cv.append(Specific_heat)
        Mag.append(Mz)
        M_sus.append(Mz_sus)

plt.xlabel('Temperature', fontsize=11)
plt.ylabel('Thermal quantities for a' + str(L) + '*' + str(L) + '*' + str(L) + ' Ising cubic system', fontsize=11)
# my_xticks = arange(0, 5.5, 0.5)
# my_yticks = arange(-2,2.5,0.5)
# xticks(my_xticks, fontsize = 9)
# yticks(my_yticks ,fontsize = 9)

plt.plot(T, E, color='r', marker='^', fillstyle='none', linestyle='none', label='MC Energy')
plt.plot(T, Cv, color='g', marker='^', fillstyle='none', linestyle='none', label='MC Specific Heat')
# plt.plot(T, Mag, color='b', marker='.', fillstyle='none', linestyle='none', label='MC Magnetization')
# plt.plot(T, M_sus, color='k', marker='.', fillstyle='none', linestyle='none', label='MC Susceptibility')

plt.xlim(0, 8.1)
# plt.ylim(-3.1, 2)
# legend(loc = 'best')
plt.show()
