import matplotlib.pyplot as plt
from numpy import linspace, exp, mod, zeros, pi, arccos, cos, sin, array
import numpy as np
import random
from tqdm import tqdm

random.seed()

"""
3D Heisenberg cubic
"""
L = 8
d = 3
N = L ** d
k = 1
J = 1


def avg_over_sites(Sx, Sy, Sz):
    E = 0
    for i in range(L):
        for j in range(L):
            for k in range(L):
                E += -J * (Sx[i, j, k] * (Sx[mod(i + 1, L), j, k] + Sx[i, mod(j + 1, L), k] + Sx[i, j, mod(k + 1, L)])
                           + Sy[i, j, k] * (Sy[mod(i + 1, L), j, k] + Sy[i, mod(j + 1, L), k] + Sy[i, j, mod(k + 1, L)])
                           + Sz[i, j, k] * (
                                       Sz[mod(i + 1, L), j, k] + Sz[i, mod(j + 1, L), k] + Sz[i, j, mod(k + 1, L)]))
    Mx = np.sum(Sx)
    My = np.sum(Sy)
    Mz = np.sum(Sz)
    M_avg = np.sqrt(Mx ** 2 + My ** 2 + Mz ** 2)

    return E/N , M_avg/N


def nn_sum(Sx, Sy, Sz, x, y, z):
    result_x = Sx[mod(x - 1, L), y, z] + Sx[mod(x + 1, L), y, z] \
               + Sx[x, mod(y - 1, L), z] + Sx[x, mod(y + 1, L), z] \
               + Sx[x, y, mod(z - 1, L)] + Sx[x, y, mod(z + 1, L)]
    result_y = Sy[mod(x - 1, L), y, z] + Sy[mod(x + 1, L), y, z] \
               + Sy[x, mod(y - 1, L), z] + Sy[x, mod(y + 1, L), z] \
               + Sy[x, y, mod(z - 1, L)] + Sy[x, y, mod(z + 1, L)]
    result_z = Sz[mod(x - 1, L), y, z] + Sz[mod(x + 1, L), y, z] \
               + Sz[x, mod(y - 1, L), z] + Sz[x, mod(y + 1, L), z] \
               + Sz[x, y, mod(z - 1, L)] + Sz[x, y, mod(z + 1, L)]

    return array([result_x, result_y, result_z])

def normal_vector():
    nx = random.random()
    ny = random.random()
    nz = random.random()
    normalize = (nx**2+ny**2+nz**2)**(1/2)
    nx /= normalize
    ny /= normalize
    nz /= normalize
    return array([nx, ny, nz])


def neighbor_spin(index, Sx, Sy, Sz, i ,j, k):
    if index == -1:
        # check behind--X axis
        Sj = array([Sx[mod(i-1, L), j, k], Sy[mod(i-1, L), j, k], Sz[mod(i-1, L), j, k]])
        return Sj, mod(i - 1, L), j, k
    if index == 1:
        Sj = array([Sx[mod(i+1, L), j, k], Sy[mod(i+1, L), j, k], Sz[mod(i+1, L), j, k]])
        return Sj, mod(i + 1, L), j, k
    if index == -2:
        # check left--Y axis
        Sj = array([Sx[i, mod(j-1, L), k], Sy[i, mod(j-1, L), k], Sz[i, mod(j-1, L), k]])
        return Sj, i, mod(j - 1, L), k
    if index == 2:
        Sj = array([Sx[i, mod(j+1, L), k], Sy[i, mod(j+1, L), k], Sz[i, mod(j+1, L), k]])
        return Sj, i, mod(j + 1, L), k
    if index == -3:
        # check underneath--Z axis
        Sj = array([Sx[i, j, mod(k-1, L)], Sy[i, j, mod(k-1, L)], Sz[i, j, mod(k-1, L)]])
        return Sj, i, j, mod(k - 1, L)
    if index == 3:
        Sj = array([Sx[i, j, mod(k+1, L)], Sy[i, j, mod(k+1, L)], Sz[i, j, mod(k+1, L)]])
        return Sj, i, j, mod(k + 1, L)


def spin_vec(Sx, Sy, Sz, i, j, k):
    return array([Sx[i, j, k], Sy[i, j, k], Sz[i, j, k]])



def cluster_flipping(Sx, Sy, Sz, Tem):
    cluster = []
    check = []
    delete_list = []
    beta = 1/Tem
    i = int(L * random.random())
    j = int(L * random.random())
    k = int(L * random.random())
    cluster.append([i, j, k])
    check.append([i, j, k])
    nor_vec = normal_vector()

    while len(check) != 0:
        index = check.pop(0)
        Si = spin_vec(Sx, Sy, Sz, index[0], index[1], index[2])
        for nn_index in [-3, -2, -1, 1, 2, 3]:
            Sj, i_prime, j_prime, k_prime = neighbor_spin(nn_index, Sx, Sy, Sz, index[0], index[1], index[2])
            p_bond_forming = 1 - np.exp(min(0, -2 * beta * Si.dot(nor_vec) * Sj.dot(nor_vec)))

            if p_bond_forming >= random.random():
                exist_count = cluster.count([i_prime, j_prime, k_prime])
                if exist_count == 0:
                    check.append([i_prime, j_prime, k_prime])
                    cluster.append([i_prime, j_prime, k_prime])



    for t in range(len(cluster)):
        new_Sx= spin_vec(Sx, Sy, Sz, cluster[t][0], cluster[t][1], cluster[t][2])[0] \
            - 2 * nor_vec[0] * spin_vec(Sx, Sy, Sz, cluster[t][0], cluster[t][1], cluster[t][2]).dot(nor_vec)

        new_Sy = spin_vec(Sx, Sy, Sz, cluster[t][0], cluster[t][1], cluster[t][2])[1] \
                 - 2 * nor_vec[1] * spin_vec(Sx, Sy, Sz, cluster[t][0], cluster[t][1], cluster[t][2]).dot(nor_vec)

        new_Sz = spin_vec(Sx, Sy, Sz, cluster[t][0], cluster[t][1], cluster[t][2])[2] \
                 - 2 * nor_vec[2] * spin_vec(Sx, Sy, Sz, cluster[t][0], cluster[t][1], cluster[t][2]).dot(nor_vec)

        Sx[cluster[t][0], cluster[t][1], cluster[t][2]] = new_Sx
        Sy[cluster[t][0], cluster[t][1], cluster[t][2]] = new_Sy
        Sz[cluster[t][0], cluster[t][1], cluster[t][2]] = new_Sz

    return Sx, Sy, Sz



Sx = zeros([L, L, L])
Sy = zeros([L, L, L])
Sz = np.ones([L, L, L])

N_tem_step = 30
N_thermalizing = 5000
Nsample = 50
Nsubsweep = N

file1 = open(r"C:\Users\a1896\OneDrive\Desktop\Comp_stat_phys\csp\ex05\3D_Heisenberg_cubic_raw\N=" + str(L) + "_cluster.txt",
             "w+")

file2 = open(r"C:\Users\a1896\OneDrive\Desktop\Comp_stat_phys\csp\ex05\3D_Heisenberg_cubic_raw\N=" + str(L) + "_cluster_Binder.txt",
             "w+")

for Tem in linspace(3.1, 0.1, N_tem_step):

    for i in tqdm(range(N_thermalizing)):
        Sx, Sy, Sz = cluster_flipping(Sx, Sy, Sz, Tem)

    M_data = zeros(Nsample)
    E_data = zeros(Nsample)

    M2_data = zeros(Nsample)
    M4_data = zeros(Nsample)

    M_data[0] = avg_over_sites(Sx, Sy, Sz)[1]
    E_data[0] = avg_over_sites(Sx, Sy, Sz)[0]

    M2_data[0] = avg_over_sites(Sx, Sy, Sz)[1] ** 2
    M4_data[0] = avg_over_sites(Sx, Sy, Sz)[1] ** 4

    for n in tqdm(range(1, Nsample)):
        for _ in range(Nsubsweep):
            Sx, Sy, Sz = cluster_flipping(Sx, Sy, Sz, Tem)
        M_data[n] = avg_over_sites(Sx, Sy, Sz)[1]
        E_data[n] = avg_over_sites(Sx, Sy, Sz)[0]

        M2_data[n] = avg_over_sites(Sx, Sy, Sz)[1] ** 2
        M4_data[n] = avg_over_sites(Sx, Sy, Sz)[1] ** 4

    M2_avg = np.mean(M2_data)
    M4_avg = np.mean(M4_data)
    M_avg = np.mean(M_data)
    M_std = np.std(M_data)
    E_avg = np.mean(E_data)
    E_std = np.std(E_data)
    Cv = N * (E_std ** 2) / (k * Tem ** 2)
    Kai = N * (M_std ** 2) / (k * Tem)

    file1.write(str(Tem) + " " + str(E_avg) + " " + str(Cv) + " " + str(M_avg) + " " + str(Kai) + "\n")
    file2.write(str(Tem) + " " + str(M2_avg**2) + " " + str(M4_avg) + "\n")

file1.close()
file2.close()

# =======================================================================================================================
# =======================================================================================================================
# =======================================================================================================================

""""""""""
READ TXT FILE
"""""""""
filename = r"C:\Users\a1896\OneDrive\Desktop\Comp_stat_phys\csp\ex05\3D_Heisenberg_cubic_raw\N=" + str(L) + "_cluster.txt"
T = []
E = []
Cv = []
Mag = []
M_sus = []
with open(filename, 'r') as file_to_read:
    data = file_to_read.readlines()
    for line in data:
        Tt, Energy, Specific_heat, Mz, Mz_sus = [float(i) for i in line.split()]
        T.append(Tt)
        E.append(Energy)
        Cv.append(Specific_heat)
        Mag.append(Mz)
        M_sus.append(Mz_sus)

plt.xlabel('Temperature', fontsize=11)
plt.ylabel('Thermal quantities for a' + str(L) + '*' + str(L) + '*' + str(L) + ' Ising cubic system', fontsize=11)
# my_xticks = arange(0, 5.5, 0.5)
# my_yticks = arange(-2,2.5,0.5)
# xticks(my_xticks, fontsize = 9)
# yticks(my_yticks ,fontsize = 9)

plt.plot(T, E, color='r', marker='^', fillstyle='none', linestyle='none', label='MC Energy')
plt.plot(T, Cv, color='g', marker='^', fillstyle='none', linestyle='none', label='MC Specific Heat')
# plt.plot(T, Mag, color='b', marker='.', fillstyle='none', linestyle='none', label='MC Magnetization')
# plt.plot(T, M_sus, color='k', marker='.', fillstyle='none', linestyle='none', label='MC Susceptibility')

plt.xlim(0, 3.2)
# plt.ylim(-3.1, 2)
# legend(loc = 'best')
plt.show()
