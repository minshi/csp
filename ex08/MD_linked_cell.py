import os
import vtktools
import numpy as np
import matplotlib.pyplot as plt
from copy import deepcopy
from tqdm import tqdm
from itertools import chain


data_dir = r"C:\Users\a1896\OneDrive\Desktop\Comp_stat_phys\csp\ex07\MD_in_cubic_box_raw\simu_link_cell"
if not os.path.exists(data_dir):
    os.makedirs(data_dir)


def r_rel_pbc(ri, rj):
    """
    Compute the relative position of particles i and j in a box
    with periodic boundary conditions.
    
    Args:
        ri: Position vector of particle i
        rj: Position vector of particle j (i must not be equal to j)
    
    Returns: 
        the shortest relative distance with correct orientation
        between particles i and j in periodic boundary conditions (PBC)
    """
    r_vec = ri - rj # relative distance without PBC
    
    for k in range(3):
        r_k = ri[k]-rj[k]
        if abs(r_k) > L/2.0:
            r_vec[k] = -np.sign(r_k)*(L - abs(r_k))
    
    return r_vec
    

def potential(ri, rj):
    """
    Args:
        ri: Position vector of particle i
        rj: Position vector of particle j (i must not be equal to j)

    Returns:
        (Lennard-Jones) potential energy between particles
    """
    r_vec = r_rel_pbc(ri, rj)
    r = np.linalg.norm(r_vec)
    
    if r > rc: # cut-off
        return 4*((1/rc)**12 - (1/rc)**6)
    else:
        return 4*((1/r)**12 - (1/r)**6)
    
    
def force(ri, rj):
    """
    Args:
        ri: Position vector of particle i
        rj: Position vector of particle j (i must not be equal to j)

    Returns:
        (Lennard-Jones) force vector
    """
    r_vec = r_rel_pbc(ri, rj)
    r = np.linalg.norm(r_vec)

    if r > rc: # cut-off
        return np.zeros(3)
    else:
        return 24*(2*(1/r)**14 - (1/r)**8)*r_vec



def energy(r_current, v_current, First_current, Next_current):
    """
    Args:
        r_current: Current particle positions
        v_current: Current particle velocities

    Returns:
        Total energy of the system
    """

    E_kin = 0.5*sum(sum(np.square(v_current)))

    # To calculate the potential, we still only consider potential inbetween particles in the neighboring cells
    E_pot = 0
    V_c = 4*((1/rc)**12-(1/rc)**6) # cutoff potential between one considered pair of particles
    particle_index = r_current / rc
    particle_index = particle_index.astype(int)

    for i in range(N):
        # first consider the potential energy due to particles in the same cell
        # since this considered particle is in this cell, so it is impossible for First_current[cell_n] to be -1
        cell_n = int(particle_index[i][0] + M * particle_index[i][1] + M ** 2 * particle_index[i][2])
        if First_current[cell_n] != i:
            # check if the considered particle is the 1st particle in the cell
            E_pot += potential(r_current[i, :], r_current[int(First_current[cell_n]), :]) - V_c
            a = int(First_current[cell_n])  # a here index the 1st particle in the cell
            while Next_current[a] != -1:
                if Next_current[a] != i:
                    # Next_current[a] here is the 2nd number in the cell
                    E_pot += potential(r_current[i, :], r_current[int(Next_current[a]), :]) - V_c
                    a = int(Next_current[a])
                    # then Next_current[a] becomes the 3rd particle in the cell
                else:
                    a = int(Next_current[a])
        else:
            a = i
            while Next_current[a] != -1:
                # Next_current[a] here is the 2nd number in the cell
                E_pot += potential(r_current[i, :], r_current[int(Next_current[a]), :]) - V_c
                a = int(Next_current[a])
        # next consider interactions in the surrounding cells
        # consider PBC
        for dx in [-1, 0, 1]:
            for dy in [-1, 0, 1]:
                for dz in [-1, 0, 1]:
                    new_x = np.mod(particle_index[i][0] + dx, M)
                    new_y = np.mod(particle_index[i][1] + dy, M)
                    new_z = np.mod(particle_index[i][2] + dz, M)
                    neighb_cell_n = int(new_x + M * new_y + M ** 2 * new_z)
                    if neighb_cell_n != cell_n:
                        if First_current[neighb_cell_n] != -1:
                            E_pot += potential(r_current[i, :], r_current[int(First_current[neighb_cell_n]), :]) - V_c
                            a = int(First_current[neighb_cell_n])
                            while Next_current[a] != -1:
                                E_pot += potential(r_current[i, :], r_current[int(Next_current[a]), :]) - V_c
                                a = int(Next_current[a])
    # then we have the total potential on each particle due to its 27 surrounding cells at each step, by going through
    # all of the particles, we get the total potential of the whole system

    return E_kin + E_pot/2



def stepVerlet(r_previous, r_current, First_current, Next_current):
    """
    Args:
        r_previous: Particle positions at time t-dt
        r_current: Particle positions at time t (it has the value: r_next as the returned quantity)

    Returns:
        Updated positions as well as velocities and forces according to the
        Verlet scheme
    """
    # compute the forces acting on every particle (=acceleration)

    r_current_pbc = r_current % L

    F = np.zeros((N, 3))
    particle_index = r_current / rc
    # put particles into corresponding small boxes
    particle_index = particle_index.astype(int)

    for i in range(N):
        # first consider the force due to particles in the same cell
        # since the considered particle is in this cell, so it is impossible for First_current[cell_n] to be -1
        cell_n = int(particle_index[i][0] + M * particle_index[i][1] + M ** 2 * particle_index[i][2])
        if First_current[cell_n] != i:
            # check if the considered particle is the first particle in that box
            F[i, :] += force(r_current_pbc[i, :], r_current_pbc[int(First_current[cell_n]), :])
            a = int(First_current[cell_n]) # a here is the number of 1st particle in the cell
            while Next_current[a] != -1:
                if Next_current[a] != i:
                    # Next_current[a] here is the 2nd and subsequent particles in the cell
                    F[i, :] += force(r_current_pbc[i, :], r_current_pbc[int(Next_current[a]), :])
                    a = int(Next_current[a])
                    # then Next_current[a] becomes the 3rd and subsequent particle in the cell
                else:
                    a = int(Next_current[a])
        else:
            a = i
            while Next_current[a] != -1:
                F[i, :] += force(r_current_pbc[i, :], r_current_pbc[int(Next_current[a]), :])
                a = int(Next_current[a])
        # next consider forces in the surronding cells
        # consider PBC
        for dx in [-1, 0, 1]:
            for dy in [-1, 0, 1]:
                for dz in [-1, 0, 1]:
                    new_x = np.mod(particle_index[i][0] + dx, M)
                    new_y = np.mod(particle_index[i][1] + dy, M)
                    new_z = np.mod(particle_index[i][2] + dz, M)
                    neighb_cell_n = int(new_x + M * new_y + M ** 2 * new_z)
                    if neighb_cell_n != cell_n:
                        # Then we have to check if there is particle in the surrounding cells
                        if First_current[neighb_cell_n] != -1:
                            F[i, :] += force(r_current_pbc[i, :], r_current_pbc[int(First_current[neighb_cell_n]), :])
                            a = int(First_current[neighb_cell_n])
                            while Next_current[a] != -1:
                                F[i, :] += force(r_current_pbc[i, :], r_current_pbc[int(Next_current[a]), :])
                                a = int(Next_current[a])
    # then we have the total force on each particle due to its 27 surrounding cells

    r_next = np.zeros((N, 3))
    del_r = np.zeros((N, 3))

    # compute the new positions using the Verlet scheme
    for i in range(N):
        r_next[i, :] = 2*r_current[i, :] - r_previous[i, :] + F[i, :]*dt**2
        del_r[i, :] = r_next[i, :] - r_previous[i, :]

        r_next[i, :] = r_next[i, :] % L

        new_index_i = r_next[i, :] / rc
        new_index_i = new_index_i.astype(int)
        new_cell_n = int(new_index_i[0] + M * new_index_i[1] + M ** 2 * new_index_i[2])
        cell_n = int(particle_index[i][0] + M * particle_index[i][1] + M ** 2 * particle_index[i][2])
        # Firstly we have to modify the index of the cell where this particle lived at the previous step. If we firstly
        # update the First & Next arrays according to the change of particle positions, Next[i] will have new indices
        # and we no longer know its original previous and next particles which have to be reconnected
        if First_current[cell_n] == i:
            # if the moved particle is the 1st particle in the original cell
            if Next_current[i] == -1:
                # and if there's no other particle in this cell
                First_current[cell_n] = -1
            else:
                # if there are some more particles
                prev_2nd_particle = np.copy(Next_current[i])
                # Next_current[i] = ?, we dont update the Next_connected number of i here, since it depends on where
                # this particle i enters
                First_current[cell_n] = prev_2nd_particle
        else:
            # if the moved particle is not the 1st particle in the original cell
            for pre_parti_index in range(N):
                # we have to connect the moved particle's previous particle index and next particle index
                if Next_current[pre_parti_index] == i:
                    Next_current[pre_parti_index] = Next_current[i]

        # Then update the First & Next array according to the change of this particle's position
        if First_current[new_cell_n] == -1:
            First_current[new_cell_n] = i
        else:
            previ_first = np.copy(First_current[new_cell_n])
            First_current[new_cell_n] = i
            Next_current[i] = previ_first


        if any(r_current[i,:] != r_current_pbc[i,:]):
            # particle i went across the boundary in the previous time step
            r_current[i,:] = r_current_pbc[i,:]
            r_next[i,:] = r_next[i,:]%L

    v_current = del_r/(2*dt)

    return r_current, v_current, r_next, F, First_current, Next_current



"""
Parameters
"""
N = 30 # particle number
L = 10 # box length
rc = 2.5 # cutoff-length
T = 1000 # simulation steps
dt = 10**-3 # time step


"""
Initialization
"""
energy_arr = np.zeros(T)

r_current = np.zeros((N, 3)) # particle positions at time t0
for i in range(N):
    r_current[i] = np.random.rand(3)*L

v_current = np.random.normal(0, 5, (N, 3)) # particle velocities at time t0
r_next = r_current + v_current*dt # particle positions at time t0+dt

"""
allocate the particles into small cells
"""
# set a cell with dim 2.5x2.5x2.5
M = L/rc
N_cell = int(M**3)
# so there are M**3 small cells, each with the side length rc
First = -np.ones(N_cell)
# the first array is to store the index of the 1st particle in each cell
Next = -np.ones(N)
# number of particle indices stored in Next may vary, so we just initiate it with -1

# if we have the location of a specific cell indicated by (i, j, k), i,j,k belong to [0,M), then the serial number of
# that cell is i + Mj + M^2k

r_next_pbc = np.mod(r_next, L)
particle_index = r_next_pbc/rc
particle_index=particle_index.astype(int)
# the particle_index stores the (i, j, k) indices of each particle

for i in range(N):
    cell_num = int(particle_index[i][0] + M * particle_index[i][1] + M**2 * particle_index[i][2])
    for j in range(N_cell):
        if cell_num == j:
            # 1st we have to check if the wanted-to-be-checked particle is in this cell or not
            if First[j] == -1:
                # we check if there is no 'first' particle in the cell j. If so, we input the particle number of the
                # particle
                First[j] = i
            else:
                # if First[j] not equal to -1, it means we have noted down the 1st particle in cell j, this is the 2nd
                # particle in this cell. But for convenience, we can set this to be the 1st one and set Next[this particle]
                # = the previous "1st particle in this cell". And in this way, we can easily elongate the particle labelling
                previous_first = np.copy(First[j])
                First[j] = i
                Next[i] = previous_first
# such that particles of the initial config have been put into the corresponding cells
# only nearest-cell interactions have to be considered
print(First)
print(Next)

vtk_writer = vtktools.VTK_XML_Serial_Unstructured()
for t in tqdm(range(T)):
    r_current, v_current, r_next, F_ij, First, Next = stepVerlet(r_current, r_next, First, Next)
    
    energy_arr[t] = energy(r_next%L, v_current, First, Next)

    r_current = r_current%L
    r_x = r_current[:, 0]
    r_y = r_current[:, 1]
    r_z = r_current[:, 2]
    F_x = F_ij[:, 0]
    F_y = F_ij[:, 1]
    F_z = F_ij[:, 2]
    vtk_writer.snapshot(os.path.join(data_dir, "MD"+str(t)+".vtu"), r_x, r_y, r_z, x_force=F_x, y_force=F_y, z_force=F_z)

vtk_writer.writePVD(os.path.join(data_dir, "MD.pvd"))

"""
Plotting the system energy
"""
plt.figure()
plt.plot(energy_arr)
#plt.ylim(0.9*np.min(energy_arr), 1.1*np.max(energy_arr))
plt.xlabel('Timesteps')
plt.ylabel('Energy')
plt.show()
