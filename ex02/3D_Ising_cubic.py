import matplotlib.pyplot as plt
from numpy import ones, linspace, exp, mod, zeros
import random
random.seed()


"""
3D Ising Cubic
"""
L = 8
d = 3
k = 1
J = 1
def threeD_single_spin_flip(state, E_initial, M_initial, Tem):
    test = state
    x = int(L * random.random())
    y = int(L * random.random())
    z = int(L * random.random())
    E_difference = 2 * J * test[x, y, z] * (test[mod(x - 1, L), y, z] + test[mod(x + 1, L), y, z]
                                            + test[x, mod(y - 1, L), z] + test[x, mod(y + 1, L), z]
                                            + test[x, y, mod(z - 1, L)] + test[x, y, mod(z + 1, L)])
    p = exp((- E_difference) / (k * Tem))
    if p >= random.random():
        state[x, y, z] *= -1
        E_initial += E_difference
        if state[x, y, z] < 0:
            M_initial -= 2
        if state[x, y, z] > 0:
            M_initial += 2
    else:
        pass
    return state, E_initial, M_initial


def thermalizing_stage(initial_state, E_initial, M_initial, Tem):
    thermo_steps = 5000 * L ** d
    for j in range(thermo_steps):
        new_state, new_E, new_M = threeD_single_spin_flip(initial_state, E_initial, M_initial, Tem)
        initial_state = new_state
        E_initial = new_E
        M_initial = new_M

    return initial_state, E_initial, M_initial


def averaging_stage(filtered_state, filtered_E, filtered_M, Tem):
    E_avg = 0.0
    E_square_avg = 0.0
    M_avg = 0.0
    M_square_avg = 0.0
    E_eq = 0.0
    M_eq = 0.0
    new_state = zeros([L, L, L], int)
    Avg_steps = 2000

    for i in range(Avg_steps):
        for j in range(L ** d):
            new_state, E_eq, M_eq = threeD_single_spin_flip(filtered_state, filtered_E, filtered_M, Tem)
            filtered_state = new_state
            filtered_E = E_eq
            filtered_M = M_eq

        E_avg += E_eq
        E_square_avg += E_eq ** 2
        M_avg += M_eq
        M_square_avg += M_eq ** 2

    return new_state, E_eq, M_eq, E_avg/Avg_steps, E_square_avg/Avg_steps, M_avg/Avg_steps, M_square_avg/Avg_steps



file1 = open(r"C:\Users\a1896\OneDrive\Desktop\Comp_stat_phys\csp\ex02\3D_Ising_cubic_raw\N=" + str(L) + ".txt", "w+")

state_initial = -ones([L, L, L], int)
E_initial = -3 * J * L ** d
M_initial = -L ** d # since we start with the simple all down initial state

for i in linspace(8, 0.01, 80):
    filtered_state, filtered_E, filtered_M = thermalizing_stage(state_initial, E_initial, M_initial, i)
    state_eq, E_eq, M_eq, E, E_2, M, M_2 = averaging_stage(filtered_state, filtered_E, filtered_M, i)
    Cv = (E_2 - E ** 2) / (k * i ** 2 * L ** d)
    Kai = (M_2 - M ** 2) / (k * i * L ** d)

    file1.write(str(i) + " " + str(E / L ** d) + " " + str(Cv) + " " + str(abs(M) / L ** d) + " " + str(Kai) + "\n")

    state_initial = state_eq
    E_initial = E_eq
    M_initial = M_eq

file1.close()

#=======================================================================================================================
#=======================================================================================================================
#=======================================================================================================================

""""""""""
READ TXT FILE
"""""""""
filename = r"C:\Users\a1896\OneDrive\Desktop\Comp_stat_phys\csp\ex02\3D_Ising_cubic_raw\N=" + str(L) + ".txt"
T = []
E = []
Cv = []
Mag = []
M_sus = []
with open(filename, 'r') as file_to_read:
    data = file_to_read.readlines()
    for line in data:
        Tt, Energy, Specific_heat, Mz, Mz_sus = [float(i) for i in line.split()]
        T.append(Tt)
        E.append(Energy)
        Cv.append(Specific_heat)
        Mag.append(Mz)
        M_sus.append(Mz_sus)

plt.xlabel('Temperature', fontsize=11)
plt.ylabel('Thermal quantities for a' + str(L) + '*' + str(L) + '*' + str(L) + ' Ising cubic system', fontsize=11)
# my_xticks = arange(0, 5.5, 0.5)
# my_yticks = arange(-2,2.5,0.5)
# xticks(my_xticks, fontsize = 9)
# yticks(my_yticks ,fontsize = 9)

plt.plot(T, E, color='r', marker='^', fillstyle='none', linestyle='none', label='MC Energy')
plt.plot(T, Cv, color='g', marker='^', fillstyle='none', linestyle='none', label='MC Specific Heat')
plt.plot(T, Mag, color='b', marker='.', fillstyle='none', linestyle='none', label='MC Magnetization')
plt.plot(T, M_sus, color='k', marker='.', fillstyle='none', linestyle='none', label='MC Susceptibility')

plt.xlim(0, 8)
plt.ylim(-3.1, 2)
# legend(loc = 'best')
plt.show()
