import matplotlib.pyplot as plt
import numpy as np
import matplotlib.cm as cm
import random
from tqdm.notebook import tqdm

"""
2d case
this is an inelastic system which will finally freeze
"""

"""
2d system
"""

def d_new(d_at_t, V_n_loc):
    return d_at_t + dt * V_n_loc

"""
The 2d code is still wrong here
"""
def main(r_initial, v_initial):
    """
    :param r_initial: position array of the particles, N x 2 in the 2d case
    :param v_initial: velocity array of the particles, N x 2 in the 2d case
    :param d_initial: relative distance array of the particles, N x N
    :return:
    """
    # check if any pair of particles collide
    d_initial = np.ones([N, N])
    for i in range(N):
        for j in range(N):
            if j != i:
                if i < j:
                    d_initial[i, j] = np.sqrt(np.sum((r_initial[i, :] - r_initial[j, :])**2)) - 2 * R_ball
    # calculate the relative velocity between each pair
    V_n_loc_free = np.zeros([N, N])
    d_v_array = np.zeros([N, 2])
    for i in range(N):
        """
        In the for loop, we only record the velocity update for each pair of colliding particles 
        After finishing the for loop, we update the positions of the whole system
        """
        for j in range(N):
            if j != i:
                """
                only half of the (i, j) pairs have to be considered since (i, j) and (j, i) are the same collision event
                and we have to avoid double counting
                """
                if i < j:
                    rij = r_initial[j, :] - r_initial[i, :]
                    norm = np.linalg.norm(rij)
                    nij = rij/norm
                    H_vec = np.array([-nij[0], -nij[1], nij[0], nij[1]]).T  # H_vec is the normal connecting vector between the particle i
                    # and j (from i to j) in the 2d case
                    v_vec = np.concatenate((v_initial[i, :], v_initial[j, :])).T
                    V_n_loc_free[i, j] = H_vec.T.dot(v_vec)
                    d_new_t = d_new(d_initial, V_n_loc_free)

                    if d_new_t[i, j] <= 0:
                        m_eff = m / 2
                        V_n_loc = - d_initial[i, j] / dt
                        R_n_loc = m_eff * (V_n_loc - V_n_loc_free[i, j]) / dt
                        R_vec = H_vec * R_n_loc
                        d_v_array[i, 0] += dt * R_vec[0] / m
                        d_v_array[i, 1] += dt * R_vec[1] / m
                        d_v_array[j, 0] += dt * R_vec[2] / m
                        d_v_array[j, 1] += dt * R_vec[3] / m

    # update the velocity and position of the whole system at the next time step
    v_initial += d_v_array
    r_initial += dt * v_initial

    # check if any particle of the system is out of the boundary
    # consider purely elastic collision with the wall
    for i in range(N):
        if r_initial[i, 0] + R_ball > L or r_initial[i, 0] - R_ball < 0:
            v_initial[i, 0] *= -1
        if r_initial[i, 1] + R_ball > L or r_initial[i, 1] - R_ball < 0:
            v_initial[i, 1] *= -1
    return r_initial, v_initial


N = 20
L = 100
R_ball = 1
m = 1
v_starting = np.random.normal(0, 40, (N, 2)) % L
dt = R_ball / np.max(v_starting) /2
# manually set the initial condition to avoid overlap at the very beginning
r_starting = np.zeros([N, 2])
for i in range(N):
    L_min = i / N * L
    L_max = (i + 1) / N * L
    r_starting[i, 0] = np.random.uniform(L_min, L_max)
    r_starting[i, 1] = np.random.uniform(L_min, L_max)



n_steps = 1000000
colors = np.linspace(0, 1, N)
cmap = cm.get_cmap('Spectral')

for i in range(n_steps):
    plt.clf()
    plt.scatter(r_starting[:, 0], r_starting[:, 1], marker='o', c=cm.jet(colors))
    plt.xlim(0, L)
    plt.ylim(0, L)
    plt.pause(10e-4)
    r_starting, v_starting = main(r_starting, v_starting)
